// ==UserScript==
// @name        Eigenartige EigenSCHAFTen
// @namespace   Soosige Soosigkeiten
// @match       *://*/*
// @grant       none
// @version     6.9.4
// @downloadURL https://gitlab.com/SuperNovaMicrowave/eigenartige-eigenSCHAFTen/-/raw/master/Eigenartige_EigenSCHAFTen.user.js
// @updateURL   https://gitlab.com/SuperNovaMicrowave/eigenartige-eigenSCHAFTen/-/raw/master/Eigenartige_EigenSCHAFTen.user.js
// @exclude     https://gitlab.com/*
// @author      AtomicToast
// @description Die Kraft kommt aus dem SCHAFT
// ==/UserScript==

var replaceTerms = [
  [/schaft/gi,'SCHAFT'],
  [/schäft/gi,'SCHÄFT'],
  [/schafft/gi,'SCHAFT'],
  [/Los/g,'Lool soos'],
  [/los/gi,'lool soos'],
  [/Wirbelsäule/g,'Würfelsäule'],
  [/Rumpf/g,'Pfmumpf'],
  [/Ausfallschritt/g,'Auaschritt'],
  [/\bnicht\b/gi,''],
  [/\bnie\b/gi,''],
  [/Unglaublich/g,'Glaublich'],
  [/unglaublich/gi,'glaublich'],
  [/Unmöglich/g,'Möglich'],
  [/unmöglich/gi,'möglich'],
  [/Betroffen/g,'Besoffen'],
  [/betroffen/gi,'besoffen'],
  [/Laufen/g,'Saufen'],
  [/laufen/gi,'saufen'],
  [/Läufer/g,'Säufer'],
  [/läufer/gi,'säufer'],
  [/unvermeidbar/g,'vermeidbar'],
  [/bereit/gi,'ＢＲＥＩＴ'],
  [/breit/gi,'ＢＲＥＩＴ'],
  [/Blick/g,'Fick'],
  [/blick/gi,'fick'],
  [/Angespannt/g,'Entspannt'],
  [/angespannt/gi,'entspannt'],
  [/Reiß/g,'Scheiß'],
  [/reiß/gi,'scheiß'],
  [/Diagnose/g,'Diagsoose'],
  [/diagnose/gi,'diagsoose'],
  [/Undenkbar/g,'Denkbar'],
  [/undenkbar/gi,'denkbar'],
  [/Unklar/g,'Klar'],
  [/unklar/gi,'klar'],
  [/Corona/g,'Ligma'],
  [/Covid-19/g,'Cockbig-19'],
  [/Experte/g,'Sexperte'],
  [/experte/gi,'sexperte'],
  [/Negativ/g,'Postiv'],
  [/negativ/gi,'postiv'],
  [/reduzieren/gi,'erhöhen'],
  [/Kaufen/g,'Saufen'],
  [/kaufen/gi,'saufen'],
  [/keine/gi,'eine'],
  [/Unbekannt/g,'Bekannt'],
  [/unbekannt/gi,'bekannt'],
  [/Fußball/g,'Fuuf'],
  [/Fussball/g,'Fuuf'],
  [/Schwierig/g,'Leicht'],
  [/schwierig/gi,'leicht']
];

var pageWalker = document.createTreeWalker (
  document.body,
  NodeFilter.SHOW_TEXT,
  {
    acceptNode: function (node) {
      if (node.nodeValue.trim())
        return NodeFilter.FILTER_ACCEPT;
      else
        return NodeFilter.FILTER_SKIP;
    }
  },
  false
);

function beschaeftigen() {
  var currentNode = null;

  while (currentNode = pageWalker.nextNode () ) {
    for (var i = 0; i < replaceTerms.length; i++) currentNode.nodeValue = currentNode.nodeValue.replace(replaceTerms[i][0], replaceTerms[i][1]);
  }
}

setTimeout(beschaeftigen, 420);
setInterval(beschaeftigen, 6900);